<?
    $secundaria=true;
    $pagina='escola';
    $titulo_pag='A Escola';
    include "includes/topo.php";
?>
    <div class="conteudo">

        <div id="titulo-pagina">
            <h1>O sonho se torna realidade</h1>
        </div>

        <div class="aescola">
            <ul class="intro-escola">
                <li class="item1">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/aescola1.jpg" alt="Foto Ballet">
                    </div>
                    <div class="texto col6 col5_md col4_sm">
                        <p>A história de amor entre Juliana e a dança começou cedo, quando ela deu seus primeiros passos disciplinados no ballet. Com o tempo e a ajuda de diversos professores, a paixão pela dança e a vontade de passar aos outros o seu conhecimento foram ficando mais claras em suas atitudes. Percebeu, então, que um sonho ali nascia.</p>
                    </div>
                </li>
                <li class="item2">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/aescola2.jpg" alt="Foto Ballet">
                    </div>
                    <div class="texto col6 col5_md col4_sm">
                        <p>A realização se deu um tempo depois. No dia 03 de Agosto de 2013, aconteceu a inauguração da tão esperada Escola de Dança. E mesmo com tanta emoção, o sonho continua se desenvolvendo. Não parar de sonhar faz com que a Juliana continue correndo atrás para alcançar todos os seus objetivos, e fazer a Escola crescer é um deles.</p>
                    </div>
                </li>
            </ul>
        </div>

    </div>
<?
    include "includes/rodape.php";
?>

