<?
    $secundaria=true;
    $pagina='contato';
    $titulo_pag='Contato';
    include "includes/topo.php";
?>
    <div class="conteudo">

        <div id="titulo-pagina">
            <h1>Entre em contato com a gente</h1>
        </div>

        <div class="formulario col12 col9_md col4_sm">
            <form id="form_contato" method="post" action="includes/envia.php" onsubmit="return checar()">
                <input type="hidden" name="destinatario" value="julianadiuana.com.br" />
                <ul class="parte1 col6 col4_md col4_sm">
                    <li>
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="nome"  placeholder="Nome" tabindex="1"/>
                    </li>
                    <li>
                        <label for="email">E-mail</label>
                        <input type="email" name="email" id="email" placeholder="E-mail" tabindex="2"/>
                    </li>
                    <li>
                        <label for="telefone">Telefone</label>
                        <input type="tel" name="telefone" id="telefone" placeholder="Telefone" maxlength="15" class="mask_telefone" tabindex="3"/>
                    </li>
                    <li>
                        <label for="telefone">Assunto</label>
                        <input type="tel" name="assunto" id="assunto" placeholder="Assunto" maxlength="15" tabindex="4"/>
                    </li>
                </ul>

                <ul class="parte2 col6 col5_md col4_sm esp no_esp_sm esp_vert_sm">
                    <li>
                        <label for="mensagem">Mensagem</label>
                        <textarea id="mensagem" name="mensagem" placeholder="Mensagem" cols="20" rows="3" tabindex="5"></textarea>
                    </li>
                    <li>
                        <button type="submit" class="botao principal" name="button" tabindex="5">Enviar</button>
                    </li>
                </ul>
            </form>
        </div>

    </div>
<?
    include "includes/rodape.php";
?>
