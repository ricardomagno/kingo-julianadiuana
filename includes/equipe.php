<div id="equipe-wrap">
	<div class="equipe">
		<h2>Os responsáveis <br>por cada objetivo alcançado</h2>

		<ul class="perfil col4 col3_md col4_sm perfil-col1">
			<li class="item-col1-1">
				<div class="over">
					<h3>Pedro Quaresma</h3>
					<p>Professor de Ballet e Contemporâneo</p>
				</div>
			</li>
			<li class="item-col1-2">
				<div class="over">
					<h3>Daniel Dias</h3>
					<p>Professor de Hip Hop</p>
				</div>
			</li>
			<li class="item-col1-3">
				<div class="over">
					<h3>Zeca</h3>
					<p>Professor de Kuduro</p>
				</div>
			</li>
		</ul>

		<ul class="perfil col4 col3_md col4_sm esp perfil-col2">
			<li class="item-col2-1">
				<div class="over">
					<h3>Juliana Diuana</h3>
					<p>Professora de Ballet e Jazz</p>
				</div>
			</li>
			<li class="item-col2-2">
				<div class="over">
					<h3>Rafaela Costa</h3>
					<p>Professora de Ballet e Jazz</p>
				</div>
			</li>
			<li class="item-col2-3">
				<div class="over">
					<h3>Geyze Faria</h3>
					<p>Professora de Ballet</p>
				</div>
			</li>
		</ul>

		<ul class="perfil col4 col3_md col4_sm esp perfil-col3">
			<li class="item-col3-1">
				<div class="over">
					<h3>Marina Callado</h3>
					<p>Professora de Ballet e Jazz</p>
				</div>
			</li>
			<li class="item-col3-2">
				<div class="over">
					<h3>Felipe Travassos</h3>
					<p>Professor de Stiletto e Jazz</p>
				</div>
			</li>
			<li class="item-col3-3">
				<div class="over">
					<h3>Bruna Lennon</h3>
					<p>Professora de Sapateado</p>
				</div>
			</li>
		</ul>
	</div>
</div>