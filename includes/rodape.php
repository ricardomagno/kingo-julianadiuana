<?
	if($pagina=="escola"){ 
		include 'includes/equipe.php';
	}
?>
</div>
<!-- end conteudo-wrap -->

<div id="rodape-wrap">
	<div class="rodape">
		<div class="parte1 col5 col3_md col4_sm">
			<h4><span>Acompanhe cada momento</span> de nossa escola!</h4>
			<ul class="social">
				<li class="item1"><a href="https://www.facebook.com/JulianaDiuanaDanca" target="_blank">Facebook</a></li>
				<li class="item2"><a href="https://www.instagram.com/julianadiuanaescoladedanca" target="_blank">Instagram</a></li>
			</ul>
		</div>
		<div class="parte2">
			<div class="texto col3 col3_md col4_sm esp_vert_sm">
				<h4>Venha nos visitar!</h4>
				<p>
					Rua Noronha Torrezão, 24<br>
					Salas 1601 | 1602 | 1603<br>
					Santa Rosa, Niterói | RJ<br>
					(21) 3254-4239
				</p>
			</div>
			<div class="img-mapa col4 col3_md col4_sm esp_vert_sm">
				<a href="https://goo.gl/maps/hocHx9CKBPF2" target="_blank">Mapa do local</a>
			</div>
		</div>
	</div>
</div>

<div id="creditos-wrap">
	<div class="creditos">
		<p class="col8 col6_md col4_sm">© 2015 | Escola de Dança Juliana Diuana - Todo o conteúdo utilizado no site é de uso exclusivo da marca.</p>
		<p class="col2 col2_md col4_sm fright esp_vert_sm"><a class="kingo" href="http://www.agenciakingo.com.br" target="_blank"></a></p>
	</div>
</div>

<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/funcoes.min.js" type="text/javascript"></script>
<!-- <script src="js/owl.carousel.min.js" type="text/javascript"></script> -->
<script src="js/smooth-scroll.min.js" type="text/javascript"></script>

</body>
</html>