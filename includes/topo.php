
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><? echo $titulo_pag; if($secundaria){ echo ' | Juliana Diuana';}?> - Escola de Dança</title>
<!-- <link href="css/reset.min.css" rel="stylesheet" type="text/css"/> -->
<link href="css/main.css" rel="stylesheet" type="text/css"/>
<!-- <link href="css/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
<link href="css/owl.theme.min.css" rel="stylesheet" type="text/css"/> -->

<meta name="description" content="" />
<link rel="shortcut icon" href="img/favicon.png" />
<link rel="apple-touch-icon" href="img/iphone.png" />
<? if(!$secundaria){echo '<link rel="canonical" href="index.php" />';}?>

<meta name="author" content="AgenciaKingo.com.br">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body class="<? if($secundaria){ echo 'secundarias '; echo $pagina; }else{ echo 'home'; } ?>">

<div id="topo-wrap">
	<div class="topo">
		<h2><a href="index.php" class="logo">Juliana Diuana</a></h2>
		<div id="navigation">
			<ul class="menu">
				<li class="item0 exibe_sm"><a href="index.php">Home</a></li>
				<li class="item1"><a href="julianadiuana.php" <?if($pagina=='juliana'){ echo 'class="ativo"';}?> >Juliana Diuana</a></li>
				<li class="item2"><a href="aescola.php" <?if($pagina=='escola'){ echo 'class="ativo"';}?> >A Escola</a></li>
				<li class="item3"><a href="modalidades.php" <?if($pagina=='modalidades'){ echo 'class="ativo"';}?> >Modalidades</a></li>
				<li class="item4"><a href="contato.php" <?if($pagina=='contato'){ echo 'class="ativo"';}?> >Contato</a></li>
			</ul>
		</div>
	</div>
</div>
<a class="icone-menu" href="javascript:;">
	<b></b>
	<b></b>
	<b></b>
</a>
<div class="nav-overlay"></div>
<div id="conteudo-wrap">