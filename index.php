<?
    $secundaria=false;
    $titulo_pag='Juliana Diuana';
    $pagina="home";
    include "includes/topo.php";
    
    if(!$secundaria){
?>
<? } ?>
    <div id="banner-wrap">
        <div class="banner">
            <div class="img-logo"></div>
            <div class="item1 box">
                <video autoplay loop poster="video/capa.jpg" class="bg_video">
                    <source src="video/video_home_1.webm" type="video/webm">
                    <source src="video/video_home_1.mp4" type="video/mp4">
                    <source src="video/video_home_1.ogg" type="video/ogg">
                </video>
            </div>
        </div>
    </div>

    <div class="conteudo">
        <div class="home">
            <div class="titulo">
                <h2>Bem vindo à <br> Juliana Diuana Escola de Dança</h2>
            </div>

            <div class="texto col6 col4_md col4_sm">
                <p>Se você já conhece, sabe. Se não conhece, precisa conhecer. Nossa Escola está situada em um ponto ótimo, em Santa Rosa, perto de tudo e dentro de um prédio comercial com segurança e cheio de outros serviços dos mais variados ramos. Espaçoso, confortável e aconchegante, o ambiente oferece comodidade e salas com qualidade impecável.</p><br>
                <p>Além do carinho, amor e atenção que são essenciais para qualquer pessoa, nossos professores são excelentes profissionais, com anos de experiência e estudo. Então venha e traga toda sua família para fazer uma aula e aproveite nossa qualidade de serviço e um lindo ambiente.</p>
            </div>

            <div class="img col6 col5_md col4_sm esp no_esp_sm esp_vert_sm">
                <img src="img/foto_home.png" alt="Foto Juliana Diuana">
            </div>
        </div>

    </div>
<?
    include "includes/rodape.php";
?>