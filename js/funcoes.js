  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '', 'auto');
  ga('require', 'displayfeatures');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');


$(document).ready(function() {

	$('a').smoothScroll();


	$(".icone-menu").click(function() {
		$('#navigation .menu').toggleClass("active");
		$('.nav-overlay').fadeToggle();
		$('.icone-menu').toggleClass("active");
	});
	$(".nav-overlay").click(function() {
		$('#navigation .menu').toggleClass("active");
		$('.nav-overlay').fadeToggle();
		$('.icone-menu').toggleClass("active");
	});


	// $(".banner").owlCarousel({
	//   slideSpeed : 300,
	//   paginationSpeed : 400,
	//   singleItem:true,
	//   autoPlay: 6000,
	//   responsive:true
	// });



});



$(document).scroll(function(){

	if(($(this).scrollTop() > 400) && ($(this).scrollTop() < 2600))
	{   
		// $('.voltar a').addClass('active');
		$('.estilo-overlay').fadeIn( "slow" );

	} else {
		$('.estilo-overlay').fadeOut();
	}

});

//verificando email válido
 function validEmail(emailAddress) {
	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return emailReg.test(emailAddress);
}




//validação formulário de contato
function checar()
{
	var i = 0;
	var j = 'Campos obrigatórios não preenchidos corretamente:\n\n';

	//alert((validEmail($("#email").val())));
	
	if (($("#nome").val().length == 0)||($("#nome").val() == 'Nome'))
	{
		j += ++i + ') Nome \n';
	}
	if (($("#email").val().length == 0)||($("#email").val() == 'Email')||($("#email").val() == 'E-mail')||(!validEmail($("#email").val())))
	{
		j += ++i + ') E-mail \n';
	}
	if (($("#telefone").val().length == 0)||($("#telefone").val() == 'Telefone'))
	{
		j += ++i + ') Telefone \n';
	}
	if (($("#assunto").val().length == 0)||($("#assunto").val() == 'Assunto'))
	{
		j += ++i + ') Assunto \n';
	}
	if (($("#mensagem").val().length == 0)||($("#mensagem").val() == 'Mensagem'))
	{
		j += ++i + ') Mensagem';
	}


	if (i > 0) 
	{
		alert(j);
		return false;
	} 
	else 
	{
		return true;
	}
}

//validação formulário de trabalhe conosco
function checar_trab()
{
	var i = 0;
	var j = 'Campos obrigatórios não preenchidos corretamente:\n\n';
	
	if (($("#nome").val().length == 0)||($("#nome").val() == 'Nome'))
	{
		j += ++i + ') Nome \n';
	}
	if (($("#email").val().length == 0)||($("#email").val() == 'Email')||($("#email").val() == 'E-mail')||(!validEmail($("#email").val())))
	{
		j += ++i + ') E-mail \n';
	}
	if (($("#telefone").val().length == 0)||($("#telefone").val() == 'Telefone'))
	{
		j += ++i + ') Telefone \n';
	}
	if (($("#portfolio").val().length == 0)||($("#portfolio").val() == 'Portfolio'))
	{
		j += ++i + ') Portfolio \n';
	}
	if (($("#mensagem").val().length == 0)||($("#mensagem").val() == 'Mensagem'))
	{
		j += ++i + ') Mensagem';
	}


	if (i > 0) 
	{
		alert(j);
		return false;
	} 
	else 
	{
		return true;
	}
}
