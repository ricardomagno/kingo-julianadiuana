<?
    $secundaria=true;
    $pagina='juliana';
    $titulo_pag='Juliana Diuana';
    include "includes/topo.php";
?>
    <div class="conteudo">

        <div id="titulo-pagina">
            <h1>Um caminho <br>de muitas conquistas</h1>
        </div>

        <div class="julianadiuana">
            <ul class="timeline">
                <li class="item1 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>1989</h3>
                        <p>Nasceu uma pequena menina chamada Juliana</p>
                    </div>
                </li>
                <li class="item2 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>1994</h3>
                        <p>Entrou na Escola de Dança Myriam Camargo, aos 4 anos</p>
                    </div>
                    <!-- <div class="img">
                        <img src="http://placehold.it/300x242" alt="Foto Ballet">
                    </div> -->
                </li>
                <li class="item3 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>1998</h3>
                        <p>Começou a estudar ballet, aos 9 anos</p>
                    </div>
                </li>
                <li class="item4 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2000</h3>
                        <p>Começou seu estudo de ballet na ponta</p>
                    </div>
                </li>
                <li class="item5 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2001</h3>
                        <p>Obteve o primeiro solo em um espetáculo de dança</p>
                    </div>
                </li>
                <li class="item6 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2003</h3>
                        <p>Teve seu primeiro papel principal, na Myriam Camargo</p>
                    </div>
                </li>
                <li class="item7 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2004</h3>
                        <p>Participou, pela primeira vez, de uma aula de Pas de Deux</p>
                    </div>
                </li>
                <li class="item8 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2004</h3>
                        <p>Estudou ballet com a Nora Esteves, no centro de movimentos Debora Colker</p>
                    </div>
                </li>
                <li class="item9 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2005</h3>
                        <p>Começou a dar aulas no Maia Vinagre</p>
                    </div>
                </li>
                <li class="item10 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2005</h3>
                        <p>Realizou seu primeiro espetáculo, criado por conta própria</p>
                    </div>
                </li>
                <li class="item11 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2006</h3>
                        <p>Fez curso de Produção e Concepção de Espetáculo, Ballet Intermediário e Jazz Avançado, em Joinville</p>
                    </div>
                </li>
                <li class="item12 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2007</h3>
                        <p>Começou a cursar dança na faculdade da Cidade (UniverCidade)</p>
                    </div>
                </li>
                <li class="item13 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2008</h3>
                        <p>Fez parte do quadro de professores da Escola de Dança Myriam Camargo</p>
                    </div>
                </li>
                <li class="item14 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2008</h3>
                        <p>Fez curso de Ballet Intermediário, Jazz Avançado e Consciência Corporal, em Joinville</p>
                    </div>
                </li>
                <li class="item15 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2010</h3>
                        <p>Começou a fazer uma pós-graduação em dança e consciência corporal</p>
                    </div>
                </li>
                <li class="item16 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2010</h3>
                        <p>Entrou na Caetano Companhia de Dança, em Niterói, e participou pela primeira vez de uma apresentação junto com o grupo</p>
                    </div>
                </li>
                <li class="item17 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2011</h3>
                        <p>Foi para Nova Iorque fazer curso, durante 2 meses. Teve aulas de ballet, coreografias de Broadway, Contemporary Jazz, entre outras</p>
                    </div>
                </li>
                <li class="item16 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2012</h3>
                        <p>Começou a dar aulas na Clarice Maia</p>
                    </div>
                </li>
                <li class="item17 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2013</h3>
                        <p>A Juliana Diuana Escola de Dança inaugurou, no dia 03/08</p>
                    </div>
                </li>
                <li class="item18 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2015</h3>
                        <p>Fez curso de Técnica de Ponta, Ballet Intermediário e Jazz Avançado, em Joinville</p>
                    </div>
                </li>
                <li class="item18 col5 col4_md col4_sm">
                    <div class="texto">
                        <h3>2015</h3>
                        <p>Criou o primeira espetáculo solo da Juliana Diuana Escola de Dança</p>
                    </div>
                </li>
            </ul>
        </div>

    </div>
<?
    include "includes/rodape.php";
?>
