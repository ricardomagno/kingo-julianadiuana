<?
    $secundaria=true;
    $pagina='modalidades';
    $titulo_pag='Modalidades';
    include "includes/topo.php";
?>
    <div class="conteudo">

        <div id="titulo-pagina">
            <h1>Vários estilos<br> para você não ficar parado</h1>
        </div>
        <? include 'includes/menu-estilo.php'; ?>

        <div class="modalidades">
            <ul class="estilos">
                <li id="ballet">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_ballet.png" alt="Foto Ballet">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Ballet</h2>
                        <p>O Ballet é um tipo de dança influente a nível mundial que possui uma forma altamente técnica e um vocabulário próprio. As turmas são separadas por idade. Sempre aumentando a dificuldade conforme vai passando de nível.</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p><strong>Baby Class | </strong>Seg a Qua - 18:15h.</p>
                            <p><strong>Pré 1 | </strong>Seg e Qua - 9:15h :: Ter e Qui - 18:15h. <strong>Pré 3 | </strong>Ter e Qui às 10h.</p>
                            <p><strong>Básico 1 | </strong>Seg e Qua - 18:15h. <strong>Básico 3 | </strong>Seg e Qua - 19:15h.</p>
                            <p><strong>Intermediário 1 | </strong>Seg e Qua - 16:30h. <strong>Intermediário 2 | </strong>Seg e Qua - 11h.</p>
                            <p><strong>Intermediário 2 (Ponta) | </strong>Sex - 10h. <strong>Intermediário 3 | </strong>Ter e Qui - 15h.</p>
                            <p><strong>Intermediário 3 (Ponta) | </strong>Sex - 16:30h.</p>
                            <p><strong>Adulto | </strong>Ter e Qui - 19:15h :: Ter e Qui - 20:15h.</p>
                            <p><strong>Adulto Iniciante | </strong>Ter e Qui - 16:30h. <strong>Avançado | </strong>Ter e Qui - 13:30h.</p>
                        </div>
                    </div>
                </li>
                <li id="jazz">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_jazz.png" alt="Foto Jazz">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Jazz</h2>
                        <p>O Jazz é uma das mais importantes formas de se expressar artisticamente, recebendo influências de diversos outros estilos e princípios técnicos do ballet e dança contemporânea.</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p><strong>Infantil | </strong>Ter e Qui - 9:15h :: Seg e Qua - 10h :: Seg e Qua - 17:15h.</p>
                            <p><strong>Adulto | </strong>Seg e Qua - 20:15h. Lirycal Jazz | Ter e Qui - 16:30h.</p>
                        </div>
                    </div>
                </li>
                <li id="contemporaneo">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_contemporaneo.png" alt="Foto Contemporâneo">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Contemporâneo</h2>
                        <p>Mais que uma técnica específica, a dança contemporânea é uma coleção de sistemas e métodos desenvolvidos a partir da dança moderna e pós-moderna, construindo, assim, uma linguagem própria, embora algumas vezes faça referência ao ballet clássico.</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p>Seg e Qua - 15:30h :: Ter e Qui - 20:15h.</p>
                        </div>
                    </div>
                </li>
                <li id="hiphop">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_hiphop.png" alt="Foto Hip Hop">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Hip Hop</h2>
                        <p>A dança Hip hop inclui uma grande variedade de estilos e o que a separa de outras formas de dança são os movimentos de improvisação.</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p>Ter e Qui - 18:15h :: Ter e Qui - 19:15h.</p>
                        </div>
                    </div>
                </li>
                <li id="sapateado">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_sapateado.png" alt="Foto Sapateado">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Sapateado</h2>
                        <p>Sapateado é um estilo de dança, originalmente irlandesa, na qual os dançarinos produzem sons sincopados, ritmados com os pés.</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p>Ter e Qui - 10h.</p>
                        </div>
                    </div>
                </li>
                <li id="stiletto">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_stiletto.png" alt="Foto Stiletto">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Stiletto</h2>
                        <p>O Stiletto foi criado, em Nova Iorque, a partir da necessidade dos bailarinos aprenderem a dançar de salto alto para apresentações em clipes, shows e comerciais. Um dos focos da modalidade é trabalhar a sensualidade e feminilidade da mulher.</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p>Seg e Qua - 19h.</p>
                        </div>
                    </div>
                </li>
                <li id="kuduro">
                    <div class="img col6 col4_md col4_sm">
                        <img src="img/foto_kuduro.png" alt="Foto Kuduro">
                    </div>
                    <div class="texto col6 col5_md col4_sm esp_vert_sm">
                        <h2>Kuduro</h2>
                        <p>texto kuduro</p>

                        <div class="horarios">
                            <h4>Horários</h4>
                            <p>Sex - 19:15h.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <!-- <div class="estilo-overlay">
            <? //include 'includes/menu-estilo.php'; ?>
        </div> -->

    </div>
<?
    include "includes/rodape.php";
?>
